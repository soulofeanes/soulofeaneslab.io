Learn more about:

- (https://markdowntutorial.com)
- GitLab Pages: [overview](https://pages.gitlab.io) and
  [documentation](https://docs.gitlab.com/ce/user/project/pages/)
- [Hugo](https://gohugo.io) static web site builder
