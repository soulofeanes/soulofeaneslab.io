+++
type = "subheader"
title = "The upcoming board election is a referendum on the soul of the Eanes School District."
+++
***This Board Election is a Referendum on the Soul of Eanes School District and our Community!***

As concerned parents, we advocate for the election of [Diane Hern](https://www.diane4eanes.com/) on May 6 for [Eanes School Board of Trustees](https://www.eanesisd.net/board). She has distinguished educational qualifications: an advanced STEM degree from UT, MBA Standford, currently studying public policy, raised a family of three Eanes ISD graduates, educational volunteerism, and engagement. We advocate for the re-election of Kim McMath and Laura Clark.

We had less time to [research the opposing candidates](https://communityimpact.com/austin/lake-travis-westlake/election/2023/03/24/4-questions-with-the-candidates-for-place-3-on-the-eanes-isd-board-of-trustees/) this year, but their slogans and strategies model restricting education in the name of safety. Worse, their campaigns give clear evidence of their public school board inexperience and a definite religious political agenda to the exclusion of other parts of our educational and religious community. Finally, the opposing candidate funding shows big money wants to control a public, educational school district that has already achieved a pinnacle of success. Oddly, the third candidate has scrubbed most information online.

Don't bet our children's education on a combined effort of two candidates with a focused religious political agenda backed by big money, the same tactic as [Stevens from 2021](/home/services/service4/). This election should not have a small turnout, but every vote matters. Early voting finishes through May 2, please support your children and [vote to elect Diane Hern on May 6](/about/)!
