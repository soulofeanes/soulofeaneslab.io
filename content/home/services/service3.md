+++
title = "Mismanagement of Funds & Incompetence"
date = "2021-03-31"
+++
Stevens was the Executive Director of CPRIT Foundation, paid her consulting firm $36,000 a month, and the State of Texas auditor demanded $327,000 returned.
<!--more-->
Administrative fees consumed more than a third of the foundation's $1 million budget last year. Most administrative fees went to the consulting firm of Jennifer Stevens, an Austin-based lobbyist and fundraiser who serves as the foundation's executive director.
--- Houston Chronicle

"This is not what I expected," said State Rep. Charles Perry, R-Lubbock. "This seems like a continuation of incompetency in the governance of money and a deliberate attempt to undermine transparency."
--- Austin Business Journal

"It seems to be working out well for the director over there. It appears that she, as head of the CPRIT Foundation, is paying herself an enormous amount of money”
--- Texas Tribune
---
![screenshot1](/AssocPress.png)
---
![screenshot2](/MySA-0.jpg)
![screenshot3](/MySA-1.jpg)
![screenshot4](/MySA-3.jpg)
![screenshot5](/MySA-4.jpg)
---
![screenshot6](/TxTrib-BigPay.png)
---
![screenshot7](/TxTribune-1.jpg)
![screenshot8](/TXTribune-2.png)
---
![screenshot9](/KMOO99.9.png)
---
![screenshot10](/BioNewsTX-1.jpg)
![screenshot11](/BioNewsTX-2.jpg)
![screenshot12](/BioNewsTX-3.jpg)
*****************************
Jennifer Stevens, the executive director of the foundation, told a House committee devoted to making agencies more transparent that the attorney general’s office and CPRIT officials were aware of the nonprofit’s plan to reconstitute itself as the Texas Cancer Coalition and turn over remaining assets to CPRIT. She said the board began discussing rebranding the organization in December. In March, the board voted to adopt the name "Texas Cancer Coalition," and to stop the practice of supplementing the salaries of CPRIT officials. The three individuals that served on both the CPRIT oversight committee and the CPRIT Foundation board — Jimmy Mansour, Joseph Bailes and Barbara Canales — have since left the nonprofit, but remain on the CPRIT oversight board.

“Over the course of the last few weeks, it has become apparent that the best course would be to wind down the foundation and give the remaining resources to the institute in support of their work,” Stevens said. The nonprofit plans to wind down and cease its operations within 60 days under the guidance of the attorney general's office. Stevens later added, “We’re trying to sort of untangle and at this point, we’re not 100 percent sure the best way to do that.”

Kristen Doyle, general counsel for the institute, told the committee that the nonprofit contacted CPRIT after filing paperwork to reconstitute itself as the Texas Cancer Coalition. “I was shocked that this was being told to us as an afterthought, several days after it happened,” said Doyle. She said the institute asked the attorney general’s office to intervene because the institute was concerned the nonprofit would spend or obligate money that should go to the institute.

State Rep. Charles Perry, R-Lubbock, said it was “disingenuous” for the nonprofit to take steps to rebrand itself as the Texas Cancer Coalition without state authorization. “There seems to be a deliberate attempt almost to continue down the road of lack of transparency,” he said. "...The truth of the matter is the history of the foundation and the institute goes a lot deeper."

Read full article: https://www.texastribune.org/2013/04/02/house-panel-questions-cprit-nonprofits-move-rebran/

---
Cancer agency's foundation's outlays questioned: https://www.houstonchronicle.com/news/houston-texas/houston/article/Cancer-agency-s-foundation-s-outlays-for-4208694.php

---

![screenshot13](/ThePaisano.png)
