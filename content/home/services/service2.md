+++
title = "Spreading Misinformation"
date = "2021-03-30"
+++

Excerpts from Stevens public record emails and Facebook posts rated as misinformation.
<!--more-->
![screenshot0](/facebook-false-information.png)

---
CDC, New England Journal of Medicine...all say to return to ALL day IN school now.
Look at the reports of cases across the country. **Are people getting sick? Some are. But they are FINE**...---Jen Stevens

December 19, 2020
Excerpts from Jennifer Stevens email to Dr. Leonard and Trustees

Austin Public Health designation of possible stage 5 is completely baseless, and you should absolutely NOT close schools or activities in EISD after the holiday break. All we are hearing lately is hyperventilation about Stage 5. What is stage 5? Stage 5 is basically Santa Claus - an imaginary invention.

+++++++
November 17, 2020
Email to Dr. Leonard and Trustees

For months we've been told to "follow the science". The science says go back to school. The science says the # of positive cases doesn't matter because the virus is 99%+ survivable. ---Jen Stevens

![screenshot2](/email-20200923.png)
![screenshot3](/email-20201130.png)
![screenshot4](/email-20201206.png)
![screenshot5](/email-20201219.png)
