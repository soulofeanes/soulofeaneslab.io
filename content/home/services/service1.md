+++
title = "Divisiveness"
date = "2021-03-29"
+++

Excerpt of emails from Stevens to Trustees showing her agenda to fire Teacher and Superintendent. Stevens has been politically funded over $99,000 for this board election.
<!--more-->

## Eanes ISD School Trustee Election:
[Campaign Finance Report for all Candidates](/TPIA7933.pdf) 5.7MB, 86 page PDF

Summary:
- Stevens funded nearly $100K, half of that comes from one couple and one individual.
- Represents over 10X more than each other candidate.
- Following the money, we conclude Stevens campaign represents a big money political agenda, not an educational agenda.

************

Excerpt from email to Trustees and Dr. Leonard - October 30, 2020
Attack on Superintendent, Dr. Leonard

The chaos of this year, the cherry picking and playing favorites, the Friday afternoon bombshell emails causing white claw-induced video rants, the hap-hazard enforcement of policies, playing favorites with teachers, punishing teachers who might be more conservative while permitting others to promote their “woke” political agenda on our kids…this is all the out of touch reality created by Dr. Leonard. He is not from here. He doesn’t have kids. He doesn’t live in the area. He needs to go.---Jen Stevens

![screenshot1](/email-20200912.png)

---

![screenshot2](/email-20201030.png)
