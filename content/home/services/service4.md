+++
title = "Big Political Money"
date = "2021-04-17"
+++
Stevens has been [funded over $99,000 for this board election](/TPIA7933.pdf), 10X more than all others, half of that came from just three people. Follow the money: Stevens was hired to control our children's future, not educate.
<!--more-->

## Eanes ISD School Trustee Election:
[Campaign Finance Report for all Candidates](/TPIA7933.pdf) 5.7MB, 86 page PDF

Summary:
- Stevens funded nearly $100K, half of that comes from one couple and one individual.
- Represents over 10X more than each other candidate.
- Following the money, we conclude Stevens campaign represents a big money political agenda, not an educational agenda.
