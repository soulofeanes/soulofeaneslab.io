+++
menu = "main"
title = "Thank You"
date = "2021-05-02"
weight = 10
draft = true
# hugo server ---buildDrafts ---buildFuture ---buildExpired
# hugo list drafts
+++
# Our Message in 2022

As concerned parents, we advocate for the re-election of [Heather Sheffield](https://heatherforeanes.com) and [Ellen Balthazar](https://www.ellenforeanes.com) on May 7 for [Eanes School Board of Trustees](https://www.eanesisd.net/board). Combined, they have over twenty years of Eanes Public School Board Trustee experience, driven our school district to be the best (#1) in Texas for several years, made our property values increase, and shown leadership excellence with the most challenging fiscal and health issues facing our district over the past few years. "You can't mess with success."

We had less time to research the opposing candidates this year, but their slogans and strategies are the same as what Balthazar and Sheffield have already delivered. Worse, their campaigns give clear evidence of their public school board inexperience and a definite religious political agenda to the exclusion of other parts of our educational and religious community. Finally, the opposing candidate funding shows big money wants to control a public, educational school district that has already achieved a pinnacle of success.

Don't bet our children's education on a combined effort of three candidates with a focused religious political agenda backed by big money, the same tactic as [Stevens from 2021](/home/services/service4/). This election will have a small turnout, so every vote matters. Early voting has runs through May 3, please support your children and [vote to re-elect Heather Sheffield and Ellen Balthazar on May 7](/about/)!

# Our Message in 2021

As concerned parents, we advocate for the re-election of [James Spradley](https://www.jamesforeanes.com/) and [Jennifer Champagne](https://jenniferchampagne.com/) on May 1 for [Eanes School Board of Trustees](https://www.eanesisd.net/board). They have reduced Eanes tax rate and built consensus with over 300 stakeholders for district improvements.

We collected the evidence below to set the record straight on candidate Jen Stevens. Stevens has no school education policy experience, [takes big political money](/home/services/service4/), and worst of all: has been embroiled in a scandal at CPRIT where she mismanaged funds with serious conflicts of interest, decision-making skills, and criticism by a former Texas Governor. Her personal attacks on Eanes educational staff shows her divisive character; she does NOT support inclusivity, diversity or equality. Her anti-science statements represent a step backwards for education.  Unlike James Spradley and Jen Champagne -- Stevens and Sprout refused to sign the Code of Fair Campaign Practices.

Don't bet our children's education on Stevens' political agenda. This election will have a small turnout, so every vote matters. Early voting has runs through April 27th, please support your children and [vote to re-elect Champagne and Spradley on May 1](/about/)!

# Thank You

Regardless of who and how you voted, our community turned out in record numbers for this local election, which represents a healthy attitude in participating in the democratic process.

The [unofficial results were posted at 10:37pm](https://countyclerk.traviscountytx.gov/images/pdfs/election_results/2021.05.01/Run_3_Cumulative_Report.pdf) after the polls had closed, both incumbent Eanes ISD Board of Trustees candidates were re-elected, much to our happiness and collective relief. A lot of hard volunteer work, word of mouth, and prayers had paid off, but each win had roughly a margin of only 10%.

There were a number of controversial political ballot measures for this election with big money media campaigns on TV, mailed flyers, and roadway signs which also drove people to the polls, so without post-poll surveys, nobody can estimate that effect.

We worked to make the soulofeanes.com web site factual by citing the original sources and using the candidates own public record and words, shining a light on the history of a particular candidate who always claimed the opposite of her record (see our analysis below). We also worked to minimize the editorial and analysis, allowing our community to evaluate and come to their own conclusions. With so much rapidly moving negativity and mis-information (bad news travels 2-5X faster than good news or retractions), we resisted a lot of the ongoing campaign and social media analysis reactions by focusing on the history of the candidates.

# Work in Progress

Now that the election is over, what follows will be more subjective and we'll work to improve and remove bias by lending evidence where possible: there is more work to be done. The post-mortem on what went well, what didn't go so well, and what could be improved is underway.

Here's a list of what didn't go so well, a lot of anecdotal evidence and subjective analysis will be presented. There is a lot of material and evidence during the campaign that we resisted commenting on, but now we can address. Our first goal is to capture topics, it will be followed by evidence, analysis, and filtering out the less strenuous proof over time.

- We didn't analyze the Stevens campaign tactics
  - Little to no educational policy:
    - Deconstructive criticism only: Stevens didn't promote a positive educational agenda because she doesn't have that experience and presumably, doesn't have that capacity.
    - Her campaign was nearly entirely negative to criticize the current board, who were often unified in deliberating the direction of Eanes. She had to criticize a united board who has led Eanes to excellence, preserved that excellence, and works to further improve it. Attacking the board was a losing proposition, so the focus had to change to personal and non-educational attacks.
  - Hypocrisy of Stevens statements, or as we like to say: J.S. B.S.
    - Xenophobia: "New York values" was uttered often, presumably as a reason why a consultant couldn't advise Eanes. Fortunately California values were not called out by Stevens because that would undermine Nigel Stout. It would have been too obvious if the consultant was criticized as being African American, so New York values would suffice.
    - Transphobia and book banning, what would follow: book burning? Censorship is a closed minded and historically anti-educational stance, especially on controversial topics that are a source of religious persecution and political divide.
    - Anti-DEI: there is no racism problem at Eanes, keeping Eanes head in the sand on this topic. Cancel the ability for Eanes students to be educated on diversity and prepare for the global business workplace.
    - "Leadership by Courage:" Stevens often pointed out, with tears in public forums, how unstable she could be when suffering attacks on her business and how that had extended to her family. Nobody condones personal attacks, but Stevens campaign continued to do just that. Courage is the ability to remain calm in difficult public circumstances, not cry. Stevens undermined her only slogan.
    - Campaign slogan rebuttals:
      - [Transparency and Consistency](https://www.jenstevens4eanes.com/transparency): "I have built successful non-profit and for-profit entities for nearly two decades"
      - COMMITMENT TO EXCELLENCE:
        - Stevens argues for "IN school learning" and that the hybrid in-person and virtual learning model is "an impossible burden on our teachers"
          - Difficult and new, but not impossible: that is a ridiculous stance by Stevens. There are some counter-examples of success. One cannot rule out possible improvements after less than a year of emergency enactment.
          - Better to work on analyzing of outcomes, which would be STAR testing students and trying to compare exclusively virtual versus in person students.
          - This proper tactic would not allow Stevens to criticize the board during the election, so the proper approach and analysis is not advocated, showing a politically motivated, weak argument.
        - Stevens advocates "Only for those rare instances of vetted medical necessity should virtual exceptions and accommodations be made"
          - Stevens wants to take away choice during a global pandemic, but gives no medical evidence to support this policy.
        - Stevens concludes "We cannot achieve excellence for every student, every teacher, every day if we are not back in school."
          - Stevens can provide no evidence for this statement.
            - Of course, every person wants to return to normal. We are nostalgic for the past and fatigued with difficulty of a global pandemic.
            - New protocols and advances in education must be prioritized, we cannot return to the past, and we must guards against every advance of the pandemic for the safety of Eanes staff, students, and their families.
            - This is a health issue for our community, which outweighs the emotional appeal and political convenience Stevens advocates.
          - Political motivation: first sentence "Eanes must stay at the head of the pack in education, extra-curricular activities, arts and sports."
            - Education is not the priority, but "extra-curricular activities, arts and sports" are asserted as equals. Why does sports suffering have priority over advancing hybrid learning models? This is political motivation and subverts education progress.
      - FISCAL RESPONSIBILITY: "I have owned my business for nearly 17 years while serving on numerous governing boards managing multi-million-dollar budgets and endowments."
        - Stevens must define success and excellence as a [disgraced, corrupt business](/home/services/service3/). Exactly the opposite of all her slogans.
        - She can prove transparency and fiscal responsibility when she is honest about failure and opens her own accounting business books.
        - We still wonder if she ever returned the money, we doubt she has. Even if she claims she has, it is not proof. If she returned the money, was it to a device of her own making, not the original sources?
  - Unethical choices
    - Stout and Stevens did not sign the ethics pledge for this election, we need to establish if that was a first time any candidate asserted their need to run an unethical campaign.
    - Bullying tactics: name calling such as "communist"
    - Spreading misinformation about other candidates
  - Raising funds over US $100K
    - We'd like to do an analysis of how it was spent, how much went to her personal discretion for spending outside the campaign, and how she profits over this campaign loss.
  - Using big money political devices:
    - More expensive, larger signs widely distributed
    - 2+ mailers to the entire community
    - Text message polls
    - Hiring canvasers from outside our community to knock on doors to promote the campaign who didn't understand the election issues
      - Hypocrite: Stevens calls for firing of Eanes staff who do not live in our community
- We didn't document Stevens campaign supporters anti-American behaviors to undermine the democratic process of voting:
  - Taking down campaign signs in neighborhoods and HOAs
  - Intimidating voters at the polling locations
  - Doxing opposing campaign volunteers
  - Spreading misinformation like their leader
- We didn't analyze endorsements by all campaigns

Here are some interesting questions about the Stevens campaign problems:

- When does a rant from an individual become fair ground for criticism, are there limits on what a candidate says when they are demonstrably lies and undermines their own creditability? We don't want crocodile tears of courage in public anymore.
-
