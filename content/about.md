+++
menu = "main"
title = "VOTE MAY 6 SATURDAY"
#date = "2021-04-19"
#type = "VOTE MAY 1"
weight = 10
+++

![about](/o-YOUNG-PEOPLE-VOTING-facebook.jpg)

**Visit** www.votetravis.com

You may vote at any Vote Center in Travis County where you see a “Vote Here/Aqui” sign.

Polls are open:
7:00 AM - 7:00 PM Monday - Saturday and Sunday, 12:00 PM - 6:00 PM

**Early Voting: April 24-May 2, 2023**

- Randalls Flagship - West Lake Hills: 3300 Bee Caves Road
- Riverbend Centre: 4214 N Capital of Texas Highway

**Election Day: Saturday, May 6, 2022, 7:00 AM - 7:00 PM**

Early Voting locations above and more:

- Austin City Hall: 301 W. Second Street

Many additional locations = https://countyclerk.traviscountytx.gov/departments/elections/current-election/#polling-locations

---
Image source: https://www.huffpost.com/entry/6-ways-the-gop-fails-to-b_b_11277412
